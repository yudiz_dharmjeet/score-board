import Card from "./Card/Card";
import CardContent from "./CardContent/CardContent";
import TeamOne from "./TeamOne/TeamOne";
import TeamTwo from "./TeamTwo/TeamTwo";
import Table from "./Table/Table";
import ChangeLangModal from "./ChangeLangModal/ChangeLangModal";

export { Card, CardContent, TeamOne, TeamTwo, Table, ChangeLangModal };
