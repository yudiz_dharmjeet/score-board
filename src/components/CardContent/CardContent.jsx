import React from "react";
import Table from "../Table/Table";
import TeamOne from "../TeamOne/TeamOne";
import TeamTwo from "../TeamTwo/TeamTwo";

import "./CardContent.scss";

function CardContent() {
  return (
    <div className="cardContent">
      <TeamOne />
      <Table />
      <TeamTwo />
    </div>
  );
}

export default CardContent;
